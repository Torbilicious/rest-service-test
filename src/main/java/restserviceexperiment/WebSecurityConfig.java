package restserviceexperiment;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

/**
 * Created by TBinder on 23/11/15.
 */

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter
{
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                    .anyRequest().permitAll()
//                    .antMatchers("/").permitAll()
//                    .antMatchers("/greeting").hasRole("USER")
//                    .antMatchers("/**").hasRole("ADMIN")
//                    .anyRequest().denyAll()
                    .and()
                .formLogin()
                    .permitAll()
                    .and()
                .logout()
                    .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                    .logoutSuccessUrl("/login")
                    .and()
                .csrf().disable();
    }

    @Configuration
    protected static class AuthenticationConfiguration extends GlobalAuthenticationConfigurerAdapter
    {
        @Value("${net.elicenser.users.admin.name}")
        private String adminUserName;
        @Value("${net.elicenser.users.admin.password}")
        private String adminUserPassword;

        @Value("${net.elicenser.users.user.name}")
        private String userUserName;
        @Value("${net.elicenser.users.user.password}")
        private String userUserPassword;

        @Override
        public void init(AuthenticationManagerBuilder auth) throws Exception {
            auth
                    .inMemoryAuthentication()
                    .withUser(adminUserName).password(adminUserPassword)
                    .roles("ADMIN", "USER").and()
                    .withUser(userUserName).password(userUserPassword)
                    .roles("USER");
        }
    }
}
