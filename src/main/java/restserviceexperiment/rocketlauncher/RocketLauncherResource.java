package restserviceexperiment.rocketlauncher;

/**
 * Created by TBinder on 24/11/15.
 */

class RocketLauncherResource
{
    String name;

    Integer right;
    Integer up;
    Integer down;
    Integer left;

    Boolean stop;
    Boolean led; // if true toggles led

    public String getName()
    {
        return name;
    }

    public Integer getRight()
    {
        return right;
    }

    public Integer getUp()
    {
        return up;
    }

    public Integer getDown()
    {
        return down;
    }

    public Integer getLeft()
    {
        return left;
    }

    public Boolean getStop()
    {
        return stop;
    }

    public Boolean getLed()
    {
        return led;
    }

    public void setRight(Integer right)
    {
        this.right = right;
    }

    public void setUp(Integer up)
    {
        this.up = up;
    }

    public void setDown(Integer down)
    {
        this.down = down;
    }

    public void setLeft(Integer left)
    {
        this.left = left;
    }

    public void setStop(Boolean stop)
    {
        this.stop = stop;
    }

    public void setLed(Boolean led)
    {
        this.led = led;
    }

    public void setName(String name)
    {
        this.name = name;
    }
}
