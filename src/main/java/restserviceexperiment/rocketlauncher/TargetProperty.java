package restserviceexperiment.rocketlauncher;

/**
 * Created by TBinder on 07/12/15.
 */
public class TargetProperty
{
    Integer right;
    Integer up;

    public void setRight(Integer right)
    {
        this.right = right;
    }

    public void setUp(Integer up)
    {
        this.up = up;
    }

    public Integer getRight()
    {
        return right;
    }

    public Integer getUp()
    {
        return up;
    }
}
