package restserviceexperiment.rocketlauncher;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import restserviceexperiment.rocketlauncher.deviceinteraction.DeviceInteractorLowLevel;

/**
 * Created by TBinder on 24/11/15.
 */

@RestController
class RocketLauncherController
{
    private final RocketLauncherService rocketLauncherService;
    private static DeviceInteractorLowLevel deviceInteractorLowLevel;

    @Autowired
    public RocketLauncherController(RocketLauncherService rocketLauncherService) throws InterruptedException
    {
        initDeviceInteraction();
        this.rocketLauncherService = rocketLauncherService;
    }

    private void initDeviceInteraction() throws InterruptedException
    {

    }

    @RequestMapping(value = "/launch", method = RequestMethod.POST)
    public HttpEntity<?> launchRocket(@RequestBody RocketLauncherResource rocketLauncherResource)
    {
        rocketLauncherService.fire(rocketLauncherResource);

        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }
}
