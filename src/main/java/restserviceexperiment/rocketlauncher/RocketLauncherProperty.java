package restserviceexperiment.rocketlauncher;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Map;
import java.util.Optional;

/**
 * Created by TBinder on 07/12/15.
 */

@ConfigurationProperties("net.elicenser")
public class RocketLauncherProperty
{
    private Map<String, TargetProperty> targets;

    public void setTargets(Map<String, TargetProperty> targets)
    {
        this.targets = targets;
    }

    public Map<String, TargetProperty> getTargets()
    {
        return targets;
    }

    public Optional<TargetProperty> getTargetForUser(String name)
    {
        return Optional.ofNullable(targets.get(name));
    }
}
