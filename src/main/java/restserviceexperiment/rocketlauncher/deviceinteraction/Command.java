package restserviceexperiment.rocketlauncher.deviceinteraction;

public class Command
{
    public final String command;
    public final Integer length;

    public Command(String command, Integer length)
    {
        this.command = command;
        this.length = length;
    }
}
