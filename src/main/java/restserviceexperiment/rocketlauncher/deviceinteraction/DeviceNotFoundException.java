package restserviceexperiment.rocketlauncher.deviceinteraction;

/**
 * Created by TBinder on 07/12/15.
 */

public class DeviceNotFoundException extends RuntimeException
{
    public DeviceNotFoundException()
    {
        super();
    }
}
