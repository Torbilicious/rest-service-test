package restserviceexperiment.rocketlauncher.deviceinteraction;

import java.util.Collections;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by TBinder on 07/12/15.
 */

public class Target
{
    public Queue<Command> commands = new LinkedList<>();

    public Target(Command[] commands)
    {
        Collections.addAll(this.commands, commands);
    }

    public Queue<Command> getCommands()
    {
        return commands;
    }
}

