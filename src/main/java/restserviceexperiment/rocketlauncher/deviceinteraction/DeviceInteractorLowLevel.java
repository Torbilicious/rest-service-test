package restserviceexperiment.rocketlauncher.deviceinteraction;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.stereotype.Service;
import org.usb4java.*;

import java.nio.ByteBuffer;
import java.util.HashMap;

/**
 * Created by TBinder on 24/11/15.
 */

@Service
public class DeviceInteractorLowLevel
{
    final short vendorId = 0x2123; //Thunder Device
    final short productId = 0x1010;

    final short vendorId2 = 0x0a81; //Original Device
    final short productId2 = 0x0701;

    public static HashMap<String, Byte[]> messages;

    private static Byte DOWN = 0x01,
            UP = 0x02,
            LEFT = 0x04,
            RIGHT = 0x08,
            FIRE = 0x10,
            STOP = 0x20,
            LEDON = 0x01,
            LEDOFF = 0x00;

    private Device device;

    private Boolean initialized = false;

    public DeviceInteractorLowLevel()
    {
        initCommands();

        Context context = new Context();
        int result = LibUsb.init(context);

        if (result != LibUsb.SUCCESS)
            throw new LibUsbException("Unable to initialize libusb.", result);


        initialized = initDevice();
    }

    private void initCommands()
    {
        Byte normalMessagePrefix = 0x02;
        Byte ledMessagePrefix = 0x03;

        messages = new HashMap<>();
        messages.put("DOWN", new Byte[]{normalMessagePrefix, DOWN});
        messages.put("UP", new Byte[]{normalMessagePrefix, UP});
        messages.put("LEFT", new Byte[]{normalMessagePrefix, LEFT});
        messages.put("RIGHT", new Byte[]{normalMessagePrefix, RIGHT});
        messages.put("FIRE", new Byte[]{normalMessagePrefix, FIRE});
        messages.put("STOP", new Byte[]{normalMessagePrefix, STOP});

        messages.put("LEDON", new Byte[]{ledMessagePrefix, LEDON});
        messages.put("LEDOFF", new Byte[]{ledMessagePrefix, LEDOFF});
    }

    public Boolean initDevice()
    {
        device = findDevice(vendorId, productId);

        if (device == null)
            device = findDevice(vendorId2, productId2);

        return device != null;
    }

    public void interact(Target target)
    {
        checkIfAlreadyInitialized();

        for (Command command : target.getCommands())
        {
            interact(messages.get(command.command));

            try
            {
                Thread.sleep(command.length);
            } catch (InterruptedException e)
            {
                e.printStackTrace();
            }

            interact(messages.get("STOP"));
        }
    }

    public void interact(Byte[] command) throws LibUsbException
    {
        checkIfAlreadyInitialized();

        DeviceHandle handle = new DeviceHandle();
        int result = LibUsb.open(device, handle);

        if (result != LibUsb.SUCCESS)
            throw new LibUsbException("Unable to open USB device", result);

        try
        {
            ByteBuffer buffer = ByteBuffer.allocateDirect(2);

            buffer.put(ArrayUtils.toPrimitive(command));

            int transfered = LibUsb.controlTransfer(handle,
                    (byte) 0x21,    // bmRequestType
                    (byte) 0x09,    // bRequest
                    (short) 0x2000, // wValue
                    (short) 0,      // wIndex
                    buffer,         // data
                    1000);          // timeout

            if (transfered < 0)
                throw new LibUsbException("Control transfer failed", transfered);

            System.out.println(String.format("%s bytes sent", transfered));
        } finally
        {
            LibUsb.close(handle);
        }
    }

    private void checkIfAlreadyInitialized()
    {
        if (!initialized)
            initialized = initDevice();

        if (!initialized)
            throw new DeviceNotFoundException();
    }

    private Device findDevice(short vendorId, short productId)
    {
        // Read the USB device list
        DeviceList list = new DeviceList();
        int result = LibUsb.getDeviceList(null, list);

        if (result < 0)
            throw new LibUsbException("Unable to get device list", result);

        try
        {
            // Iterate over all devices and scan for the right one
            for (Device device : list)
            {
                DeviceDescriptor descriptor = new DeviceDescriptor();
                result = LibUsb.getDeviceDescriptor(device, descriptor);

                if (result != LibUsb.SUCCESS)
                    throw new LibUsbException("Unable to read device descriptor", result);

                if (descriptor.idVendor() == vendorId && descriptor.idProduct() == productId)
                {
                    System.out.println(String.format("Device Found! %s", device));
                    return device;
                }
            }
        } finally
        {
            // Ensure the allocated device list is freed
            LibUsb.freeDeviceList(list, true);
        }

        // Device not found
        return null;
    }
}
