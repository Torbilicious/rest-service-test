package restserviceexperiment.rocketlauncher;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import restserviceexperiment.rocketlauncher.deviceinteraction.Command;
import restserviceexperiment.rocketlauncher.deviceinteraction.DeviceInteractorLowLevel;
import restserviceexperiment.rocketlauncher.deviceinteraction.Target;

import java.util.Optional;
import java.util.function.Consumer;

/**
 * Created by TBinder on 24/11/15.
 */

@Service
class RocketLauncherService
{
    private final DeviceInteractorLowLevel deviceInteractor;
    private final RocketLauncherProperty rocketLauncherProperty;

    @Autowired
    public RocketLauncherService(DeviceInteractorLowLevel deviceInteractor, RocketLauncherProperty rocketLauncherProperty)
    {
        this.deviceInteractor = deviceInteractor;
        this.rocketLauncherProperty = rocketLauncherProperty;
    }

    public void fire(RocketLauncherResource resource)
    {
        Optional<TargetProperty> targetProperty = rocketLauncherProperty.getTargetForUser(resource.getName());

        targetProperty.ifPresent(targetProperty1 -> {

            Target target = new Target(new Command[]{new Command("RIGHT", targetProperty1.getRight()),
                    new Command("UP", targetProperty1.getUp()),
                    new Command("FIRE", 4500)});

            deviceInteractor.interact(target);

            reset();
        });
    }

    private void reset()
    {
        Target resetTarget = new Target(new Command[]{new Command("DOWN", 2000),
                new Command("LEFT", 8000)});

        deviceInteractor.interact(resetTarget);
    }
}
