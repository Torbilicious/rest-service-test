package restserviceexperiment.fibonachi;

import org.springframework.stereotype.Service;

/**
 * Created by TBinder on 23/11/15.
 */

@Service
class FibonachiService
{
    public Long getFibonachiAt(Long number)
    {
        if (number <= 0)
            return 0L;
        else if (number == 1)
            return 1L;
        else
        {
            Long a = 0L; // hat am Anfang der Schleife den Wert Fib(i-2)
            Long b = 1L; // hat am Anfang der Schleife den Wert Fib(i-1)
            Long i = 2L;

            while (i <= number) // Schleife fuer alle Werte von 2 bis n
            {
                Long aa = b; // Wert von Fib(i-1)
                Long bb = a + b; // Wert von Fib(i)

                a = aa; // Vorbereitung fuer den naechsten Durchgang
                b = bb; // Vorbereitung fuer den naechsten Durchgang

                i++;
            }

            return b;
        }
    }
}
