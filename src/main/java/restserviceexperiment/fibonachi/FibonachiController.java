package restserviceexperiment.fibonachi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by TBinder on 23/11/15.
 */

@RestController
class FibonachiController
{
    private static final String template = "FibonachiService number at %s is %s";
    private final FibonachiService fibonachiService;

    @Autowired
    public FibonachiController(FibonachiService fibonachiService)
    {
        this.fibonachiService = fibonachiService;
    }

    @RequestMapping("/fib")
    public String fibonachi(@RequestParam(value = "number", defaultValue = "1") Long number)
    {
        long result = fibonachiService.getFibonachiAt(number);

        return String.format(template, number, result);
    }
}
