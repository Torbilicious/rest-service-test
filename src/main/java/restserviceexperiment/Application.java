package restserviceexperiment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import restserviceexperiment.rocketlauncher.RocketLauncherProperty;

@SpringBootApplication
@EnableConfigurationProperties(RocketLauncherProperty.class)
public class Application
{
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
