package restserviceexperiment.home;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by TBinder on 23/11/15.
 */

@RestController
class HomeController
{
    @RequestMapping("/")
    public String index()
    {
        return "Welcome to the home page!";
    }
}
