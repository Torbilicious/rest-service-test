package restserviceexperiment.devicedetection;

import org.springframework.mobile.device.Device;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by TBinder on 23/11/15.
 */

@RestController
class DeviceDetectionController
{
    @RequestMapping("/detect")
    public @ResponseBody String detectDevice(Device device) {
        String deviceType = "unknown";
        if (device.isNormal()) {
            deviceType = "normal";
        } else if (device.isMobile()) {
            deviceType = "mobile";
        } else if (device.isTablet()) {
            deviceType = "tablet";
        }

        return String.format("Hello %s browser! On %s", deviceType, device.getDevicePlatform());
    }
}
